FROM openjdk:8-jdk-alpine
VOLUME /tmp
EXPOSE 3002
ADD ./target/ramosvji-zuul-0.0.1-SNAPSHOT.jar /ramosvji-zuul.jar
ENTRYPOINT ["java","-jar","/ramosvji-zuul.jar","com.ramosvji.zuul.RamosvjiZuulApplication"]